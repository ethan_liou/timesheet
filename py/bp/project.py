from flask import Blueprint, request, g, jsonify, session
from db import ProjectMember, ProjectMaster, MemberMaster, session_scope

project_bp = Blueprint('project_bp', __name__)

@project_bp.route('/list')
def list_user_project():
    fields = request.args.getlist('field')
    projects = {}
    with session_scope(g.db.Session) as s:
        for project in s.query(ProjectMaster).join(ProjectMember).filter(ProjectMember.member_id==session["userid"]):
            js = {}
            for field in fields:
                if hasattr(project, field):
                    js[field] = getattr(project, field)
            projects[project.project_id] = js
    return jsonify(projects)

@project_bp.route('/members/<project_id>')
def list_members(project_id):
    members = {}
    with session_scope(g.db.Session) as s:
        for member in s.query(MemberMaster).join(ProjectMember).filter(ProjectMember.project_id==project_id):
            members[member.member_id] = {"name":member.member_name, "self":member.member_id == session["userid"]}
    return jsonify(members)

@project_bp.route('/add', methods=["POST"])
def add_project():
    obj = request.get_json(force=True)
    project_id = None
    with session_scope(g.db.Session) as s:
        project = ProjectMaster(project_name=obj["name"])
        s.add(project)
        s.commit()
        project_id = project.project_id

        # join
        member = ProjectMember(project_id = project_id, member_id = session["userid"])
        s.add(member)
        s.commit()
    return jsonify({"project_id":project_id})

@project_bp.route('/join')
def join_project():
    projectid = request.args.get("projectid")
    with session_scope(g.db.Session) as s:
        member = ProjectMember(project_id = project_id, member_id = session["userid"])
        s.add(member)
        s.commit()
    return ""