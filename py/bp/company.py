from flask import Blueprint, request, g, jsonify, session
from db import MemberMaster, session_scope

company_bp = Blueprint('company_bp', __name__)

@company_bp.route('/list_members')
def list_members():
    fields = request.args.getlist('field')
    members = {}
    with session_scope(g.db.Session) as s:
        for member in s.query(MemberMaster):
            js = {"self":member.member_id == session["userid"]}
            for field in fields:
                if hasattr(member, field):
                    js[field] = getattr(member, field)
            members[member.member_id] = js
    return jsonify(members)
