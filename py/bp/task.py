from flask import Blueprint, request, g, jsonify, session
from db import TaskMaster, session_scope, MemberMaster
import requests

task_bp = Blueprint('task_bp', __name__)

@task_bp.route('/list')
def list_user_task():
    # support filter : project_id, task_owner_id
    fields = request.args.getlist('field')
    tasks = {}

    args = dict()
    if "s" in request.args:
        args["task_owner_id"] = session["userid"]
    if "task_owner_id" in request.args:
        args["task_owner_id"] = request.args["task_owner_id"]
    if "project_id" in request.args:
        args["project_id"] = request.args["project_id"]

    with session_scope(g.db.Session) as s:
        cursor = s.query(TaskMaster).filter_by(**args)
        if "status" in request.args:
            cursor = cursor.filter(TaskMaster.status.in_(request.args['status'].split(",")))
        for task in cursor:
            js = {}
            if len(fields) > 0:
                for field in fields:
                    if hasattr(task, field):
                        js[field] = getattr(task, field)
            else:
                js = task.asDict()
            tasks[task.task_id] = js
    return jsonify(tasks)

@task_bp.route('/add', methods=["POST"])
def add_task():
    obj = request.get_json(force=True)
    task_id = None
    with session_scope(g.db.Session) as s:
        task = TaskMaster(task_creator_id=session["userid"], task_owner_id=session["userid"])
        if obj["requester"] == -1:
            obj["requester"] = session["userid"]
        for k, v in obj.iteritems():
            if hasattr(task, k) and k != "task_id":
                if type(v) == unicode:
                    v = v.encode("utf8")
                setattr(task, k, v)
        s.add(task)
        s.commit()
        task_id = task.task_id
    return jsonify({"task_id":task_id})

@task_bp.route('/change_attr', methods=["POST"])
def change_attr():
    obj = request.get_json(force=True) # { task_id:yyy, attr:xxx}
    with session_scope(g.db.Session) as s:
        task = s.query(TaskMaster).filter_by(task_id=obj["task_id"]).update(obj["attr"])
        if obj["attr"].get("status", "na") == "done":
            task = s.query(TaskMaster).filter_by(task_id=obj["task_id"]).one()
            requests.post("https://gogoyc.slack.com/services/hooks/slackbot?token=QLrAzee6GLSrGLhNiyctPtzS&channel=%23bugreport", data="Awesome!! {} finished {}".format(task.task_owner.member_name, task.task_name))
        s.commit()
    return jsonify({"status":"OK"})
