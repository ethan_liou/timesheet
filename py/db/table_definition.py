from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
import datetime

DBase = declarative_base()

class Base(DBase):
    __abstract__ = True
    def asDict(self):
        d = {}
        for column in self.__table__.columns:
            d[column.name] = getattr(self, column.name)
        return d

class UserMaster(Base):
    __tablename__ = "user_master"
    __table_args__ = {'schema': 'global'}
    user_id = Column(Integer, primary_key=True) 
    email = Column(String(100))
    create_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))

    # relation
    joined_company = relationship("MemberMaster", backref="user")

    def __repr__(self):
        return "<UserMaster(id='{}', email='{}', create_time='{}')>".format(self.user_id, self.email, self.create_time)

class MemberMaster(Base):
    __tablename__ = "member_master"
    member_id = Column(Integer, primary_key=True)
    global_id = Column(Integer, ForeignKey(UserMaster.user_id))
    member_name = Column(String(100), index=True)
    join_date = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))

    # relation
    owned_tasks = relationship("TaskMaster", backref="task_owner", foreign_keys="TaskMaster.task_owner_id")
    created_tasks = relationship("TaskMaster", backref="task_creator", foreign_keys="TaskMaster.task_creator_id")
    projects = relationship("ProjectMember", backref="member")

    def __repr__(self):
        return "<MemberMaster(id='{}', name='{}', join_date='{}')>".format(self.member_id, self.member_name, self.join_date)

class TaskMaster(Base):
    __tablename__ = "task_master"
    task_id = Column(Integer, primary_key=True)
    task_name = Column(String(250))
    project_id = Column(Integer, ForeignKey('project_master.project_id'), server_default="0")
    task_owner_id = Column(Integer, ForeignKey('member_master.member_id'))
    task_creator_id = Column(Integer, ForeignKey('member_master.member_id'))
    task_deadline = Column(DateTime)
    create_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    priority = Column(Integer, server_default="1")
    time_allocation = Column(Float, server_default="0")
    status = Column(Enum("doing", "todo", "deleted", "done"), server_default="todo")
    enabled = Column(Boolean, server_default="0")
    requester = Column(Integer, ForeignKey('member_master.member_id'))

    def __repr__(self):
        return "<TaskMaster(id='{}', name='{}', create_time='{}' status='{}')>".format(self.task_id, self.task_name, self.create_time, self.status)

class ProjectMaster(Base):
    __tablename__ = "project_master"
    project_id = Column(Integer, primary_key=True)
    project_name = Column(String(250))
    create_time = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))

    # relation
    tasks = relationship("TaskMaster", backref="project")
    members = relationship("ProjectMember", backref="project")

    def __repr__(self):
        return "<ProjectMaster(id='{}', name='{}', create_time='{}')>".format(self.project_id, self.project_name, self.create_time)

class ProjectMember(Base):
    __tablename__ = "project_member"
    project_id = Column(Integer, ForeignKey(ProjectMaster.project_id), primary_key=True)
    member_id = Column(Integer, ForeignKey(MemberMaster.member_id), primary_key=True)
    join_date = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    leave_date = Column(DateTime)

    def __repr__(self):
        return "<ProjectMember(p_id='{}', m_id='{}', join_date='{}')>".format(self.project_id, self.member_id, self.join_date)

