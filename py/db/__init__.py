from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
from table_definition import *
from contextlib import contextmanager
import datetime
from sqlalchemy.orm.exc import NoResultFound

@contextmanager
def session_scope(factory):
    session = factory()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()

class DBContext(object):
    def __init__(self, db_name):
        engine = create_engine('mysql+pymysql://webapi:28ed80db3d9519ffe18d4f114970d488@timesheet.czqea1r2j8b8.ap-northeast-1.rds.amazonaws.com/{}'.format(db_name), pool_recycle=3600)
        self.__Session = sessionmaker(engine)
        if not database_exists(engine.url):
            create_database(engine.url)
        Base.metadata.bind = engine
        Base.metadata.create_all()

    @property
    def Session(self):
        return self.__Session

def upsert_account_if_need(context, email):
    with session_scope(context.Session) as session:
        member = None
        try:
            member = session.query(MemberMaster).join(UserMaster).filter(UserMaster.email==email).one()
        except NoResultFound:
            user = UserMaster(email=email)
            session.add(user)
            session.commit()

            # add to member
            member = MemberMaster(global_id=user.user_id, member_name=email.split("@")[0].capitalize())
            session.add(member)
            session.commit()
        return member.member_id

if __name__ == "__main__":
    context = DBContext("testa")
    upsert_account_if_need(context, "ethanliou@gogolook.com")
