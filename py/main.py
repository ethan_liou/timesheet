from flask import Flask, request, send_from_directory, jsonify, g, session, redirect, flash
import requests
from db import DBContext, upsert_account_if_need

app = Flask(__name__, static_folder='../build')
app.debug=True
dbContext = DBContext("testa")

CLIENT_ID = "1015304951342-g32lkvtimcf3rppvb47v54t48f7kqfh5.apps.googleusercontent.com"
CLIENT_SEC = "yEdffRKvyNfsDc8WnTxV7u16"
app.secret_key = "ovnqweo;ifnwqeo;incwq;oeinreioqwtmqw;"

@app.before_request
def before_request():
    g.db = dbContext

# oauth
@app.route('/oauthback')
def oauth():
    res = requests.post("https://www.googleapis.com/oauth2/v3/token", data=dict(
        code=request.args["code"],
        client_id=CLIENT_ID,
        client_secret=CLIENT_SEC,
        grant_type="authorization_code",
        redirect_uri=request.url_root + "oauthback"
    ))
    if res.status_code == 200:
        js = res.json()
        people = requests.get(
            "https://www.googleapis.com/userinfo/email?alt=json&access_token={}".format(js["access_token"]))
        if people.status_code == 200:
            mail = people.json()["data"]["email"]
            userid = upsert_account_if_need(g.db, mail)
            session["userid"] = userid 
            return redirect("index.html")
    flash('Log in fail. Please try again.')
    return redirect("landing.html")

@app.route("/logout")
def logout():
    session.pop('userid', None)
    return redirect("landing.html")

@app.route("/login", methods=['POST'])
def login():
    return redirect(("https://accounts.google.com/o/oauth2/auth?" +
                     "scope=email&prompt=select_account&response_type=code&client_id={}&" +
                     "redirect_uri={}oauthback").format(CLIENT_ID,
                                                        request.url_root))    

@app.route('/')
@app.route('/<path:path>')
def static_proxy(path="index.html"):
    if path == "index.html" and session.get("userid", None) is None:
        return redirect("landing.html") 
    else:
        return app.send_static_file(path)

# Register blueprint
from bp.task import task_bp
app.register_blueprint(task_bp, url_prefix='/task')
from bp.project import project_bp
app.register_blueprint(project_bp, url_prefix='/project')
from bp.company import company_bp
app.register_blueprint(company_bp, url_prefix='/company')

if __name__ == "__main__":
    app.run()