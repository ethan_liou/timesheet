var gulp = require('gulp');
var rsync = require('gulp-rsync');
var GulpSSH = require('gulp-ssh');
var fs = require('fs');
var debug = require('gulp-debug');
var uglify = require('gulp-uglify');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');

var config = {
  host: 'timesheet',
  port: 22,
  username: 'ubuntu',
  privateKey: fs.readFileSync('/Users/Ethan/timesheet/timesheet.pem')
}
 
var gulpSSH = new GulpSSH({
  ignoreErrors: false,
  sshConfig: config
})

gulp.task('compress', function(){
  return gulp.src('build/*.js')
    .pipe(uglify())
    .pipe(clean())
    .pipe(gulp.dest('build'));
});

gulp.task('rsync_html', function() {
  return gulp.src('build/**')
    .pipe(rsync({
      username: 'ubuntu',
      recursive: true,
      hostname: 'timesheet',
      destination: '~/www'
    }));
});

gulp.task('rsync_py', function() {
  return gulp.src('py/**')
    .pipe(rsync({
      username: 'ubuntu',
      recursive: true,
      hostname: 'timesheet',
      exclude: ['*.pyc'],
      destination: '~/www'
    }));
});

gulp.task('move', function () {
  return gulpSSH
    .shell(['sudo rsync -av --delete /home/ubuntu/www/ /var/www/', 'cd /var/www', 'sudo chown -R www-data:www-data *']);
});

gulp.task('restart_service', function () {
  return gulpSSH
    .shell(['~/venv/bin/pip install -r ~/www/py/requirement.txt', 'sudo restart uwsgi']);
});

gulp.task('deploy', function(done) {
    runSequence('build', 'compress', 'rsync_html', 'rsync_py', 'move', 'restart_service', 'slack', function() {
        done();
    });
});
