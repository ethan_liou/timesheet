var React = require('react');
var PropTypes = React.PropTypes;
var mui = require('material-ui');
var Colors = mui.Styles.Colors;
var { Spacing, Colors } = mui.Styles;
var moment = require('moment');
var {
  ClearFix,
  Card,
  CardHeader,
  CardText,
  CardActions,
  CardTitle,
  RaisedButton,
  FlatButton,
  Avatar,
  Paper
} = mui;
var { DragSource, DropTarget } = require('react-dnd');
var DragItem = require('../constants/DragItem');
var TaskAction = require('../actions/TaskAction');
var CompanyStore = require("../stores/CompanyStore");
var PriorityDiv = require("./priority_div.jsx");
var StatusDiv = require("./status_div.jsx");

var UserTask = React.createClass({
    propTypes: {
      connectDragSource: PropTypes.func.isRequired,
      isDragging: PropTypes.bool.isRequired,

      connectDropTarget: PropTypes.func.isRequired,
      isOver: PropTypes.bool.isRequired,
      canDrop: PropTypes.bool.isRequired,

      userview: PropTypes.bool.isRequired
    },
    getStyles(){
      return {
        avatar:{
          borderRadius: "21px",
          border:"transparent 1px dashed"
        },
        doingAvatar:{
          borderRadius: "20px",
          border:"red 2px solid"
        },
        emptyAvatar:{
          borderRadius: "21px",
          border:"#AAAAAA 1px dashed",
          backgroundColor:"transparent"
        }
      };
    },
    getInitialState(){
      return {};
    },
    _onChange(){
      CompanyStore.removeChangeListener(this._onChange);
      this.forceUpdate();
    },
    componentDidMount() {
      if($$$$.isEmptyObject(CompanyStore.getMembers())){
        CompanyStore.addChangeListener(this._onChange);
      }
    },
    _switchEnabled(task){
      TaskAction.change_attr(task, "enabled", !task.enabled);
    },
    render() {
      var styles = this.getStyles();
      var username = "";
      var owner = "";
      if(!$$$$.isEmptyObject(CompanyStore.getMembers())){
        var style = styles.emptyAvatar;
        if(this.props.task.task_owner_id != 0){
          style = styles.avatar;
        }
        if(this.props.isOver){
          style.backgroundColor = "red";
        }
        username = CompanyStore.getMember(this.props.userview ? this.props.task.requester : this.props.task.task_owner_id).member_name;
        owner = <Avatar style={style}>{username[0]}</Avatar>;
      }
      var style = {...this.props.style, padding:"10px", opacity: this.props.isDragging ? 0 : 1,  };
      if(this.props.task.enabled){
        style.border = "red 2px solid";
      } else {
        style.border = "transparent 2px solid";
      }
      var rightInfo = this.props.userview ? <PriorityDiv priority={this.props.task.priority}/> : <StatusDiv status={this.props.task.status}/>;
      return this.props.connectDropTarget(this.props.connectDragSource(
        <div className="card-panel hoverable" onClick={this.props.onClick} onDoubleClick={this._switchEnabled.bind(null, this.props.task)} style={style}>
          <table width="100%">
            <tr>
              <td style={{verticalAlign:"top", paddingTop:"0px"}} width="42px">{owner}</td>
              <td style={{paddingLeft:"4px", paddingTop:"0px"}}>
                <div>{username}</div>
                <div style={{ color:"gray"}}>{this.props.task.task_name}</div>
              </td>
            </tr>
          </table>
          <ClearFix>
            <div style={{float:"left"}}>{moment(new Date(this.props.task.task_deadline)).fromNow()}</div>
            <div style={{float:"right"}}>{rightInfo}</div>
          </ClearFix>
        </div>));
    }
});

var spec = {
    beginDrag: function (props, monitor, component) {
      return { ...props.task, size:{width:component.getDOMNode().offsetWidth, height:component.getDOMNode().offsetHeight}};
    },
    endDrag: function(props, monitor) {
      var res = monitor.getDropResult();
      if(res){
        var attr_k = null;
        var attr_v = null;
        for(var k in res.action){
            attr_k = k;
            attr_v = res.action[k];
        }
        TaskAction.change_attr(monitor.getItem(), attr_k, attr_v);
      }
    }
}

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    };
}

var memberSpec = {
    drop: function (props, monitor, component) {
      return {task: props.task};
    }
}

function memberCollect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}


module.exports = DropTarget(DragItem.MEMBER, memberSpec, memberCollect)(DragSource(DragItem.TASK, spec, collect)(UserTask));
