var React = require('react');
var Router = require('react-router');
var { AppCanvas, IconButton, Menu, Styles, FlatButton } = require('material-ui');
var RouteHandler = Router.RouteHandler;
var { Colors, Spacing } = Styles;
var ThemeManager = new Styles.ThemeManager();
var DragDropContext = require('react-dnd').DragDropContext;
var HTML5Backend = require('react-dnd/modules/backends/HTML5');
var AppLeftNav = require("./app_leftnav.jsx"); 
var TaskAction = require('../actions/TaskAction');
var CompanyAction = require('../actions/CompanyAction');
var ProjAction = require('../actions/ProjAction');
var AppBar = require("./appbar.jsx");
var CompanyStore = require('../stores/CompanyStore');
var ProjStore = require('../stores/ProjStore');

var Master = React.createClass({
  childContextTypes: {
    muiTheme: React.PropTypes.object,
  },
  getStyles(){
    return {
      leftBar:{
        backgroundColor:"#221E1D",
        position:'fixed',
        top: '0px',
        left: '0px',
        bottom: '0px',
        width: '220px',
        overflowX: "hideen",
        overflowY: "scroll"
      },
      root:{
        paddingLeft: '220px',
        marginLeft:'20px',
        marginRight:'20px',
        minWidth:'600px'
      }
    }
  },
  getChildContext() {
    var theme = ThemeManager.getCurrentTheme();
    return {
      muiTheme: theme
    };
  },
  contextTypes:{
    router: React.PropTypes.func
  },
  getInitialState(){
    return {projects:null, members:null};
  },
  _onProjBack(){
    ProjStore.removeChangeListener(this._onProjBack);
    var projects = $$$$.map(ProjStore.getAll(), function(project, pid){
      return [{...project, project_id:pid}];
    });
    setTimeout(function(){
      this.setState({projects:projects});
    }.bind(this), 0)
  },
  _onMemberBack(){
    CompanyStore.removeChangeListener(this._onMemberBack);
    var members = $$$$.map(CompanyStore.getMembers(), function(member, mid){
      return [{...member, member_id:mid}];
    });
    setTimeout(function(){
      this.setState({members:members});
    }.bind(this), 0)
    
  },
  componentDidMount() {
    ProjStore.addChangeListener(this._onProjBack);
    CompanyStore.addChangeListener(this._onMemberBack);
    ProjAction.list();
    CompanyAction.listMembers();
  },
  render() {
    var styles = this.getStyles();
    return this.state.projects && this.state.members ? (
      <div>
        <AppLeftNav style={styles.leftBar}/>
        <div style={styles.root}>
          <RouteHandler />
        </div>
      </div>
    ) : (<h1>Loading...</h1>);
  }
});

module.exports = DragDropContext(HTML5Backend)(Master);