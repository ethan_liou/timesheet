var React = require('react');
var update = require('react/lib/update');
var mui = require('material-ui');
var ThemeManager = new mui.Styles.ThemeManager();
var Colors = mui.Styles.Colors;
var { Spacing, Colors } = mui.Styles;
var {
  ClearFix,
  Card,
  CardHeader,
  Avatar,
  CardText,
  CardActions,
  FlatButton,
  RaisedButton,
  FloatingActionButton,
  CardTitle,
} = mui;
var Add = require('material-ui/lib/svg-icons/content/add');
var Clear = require('material-ui/lib/svg-icons/content/clear');
var Done = require('material-ui/lib/svg-icons/action/done');
var AddTaskDialog = require('./add_task_dlg.jsx');
var ToastView = require("./toast_view.jsx");
var TaskContainer = require("./task_container.jsx");
var Droppable = require("./droppable.jsx");
var TaskAction = require("../actions/TaskAction");
var ProjAction = require("../actions/ProjAction");
var ProjStore = require("../stores/ProjStore");
var MemberBar = require("./member_bar.jsx");

module.exports = React.createClass({
  getStyles(){
    return {
      card:{
        margin:"5px"
      },
      addBtn:{
        position:'fixed',
        right: '24px',
        bottom: '24px'
      },
      removeBtn:{
        position:'fixed',
        right: '124px',
        bottom: '24px'
      },
      doneBtn:{
        position:'fixed',
        right: '24px',
        bottom: '24px'
      }
    };
  },
  _addTask(){
    this.refs.add_task_dlg.show();
  },
  _reloadData(projId){
    TaskAction.list({project_id:projId, status:"todo,doing"});
    ProjAction.listMembers(projId);
  },
  componentWillMount(){
    this._reloadData(this.props.params.id);
  },
  componentWillReceiveProps(nextProps){
    this._reloadData(nextProps.params.id);
  },
  _editTask(task){
    this.refs.add_task_dlg.show(task);
  },
  render() {
    var styles = this.getStyles();
    return (
      <div>
        <h5>Project : {ProjStore.getProject(this.props.params.id).project_name}</h5>
        <div className="divider"></div>
        <FloatingActionButton style={styles.addBtn} onClick={this._addTask}>
          <Add />
        </FloatingActionButton>
        <Droppable eleStyle={styles.removeBtn} action="done">
          <Done />
        </Droppable>
        <Droppable eleStyle={styles.doneBtn} action="deleted">
          <Clear />
        </Droppable>
        <ToastView/>
        <div className="row">
          <div className="col s4">
            <h6>High Priority</h6>
            <TaskContainer onClickItem={this._editTask} userview={false} filter={{priority:3}}/>
          </div>
          <div className="col s4">
            <h6>Mid Priority</h6>
            <TaskContainer onClickItem={this._editTask} userview={false} filter={{priority:2}}/>
          </div>
          <div className="col s4">
            <h6>Low Priority</h6>
            <TaskContainer onClickItem={this._editTask} userview={false} filter={{priority:1}}/>
          </div>
        </div>
        <AddTaskDialog ref="add_task_dlg" project_id={this.props.params.id}/>
      </div>
    );
  }
});