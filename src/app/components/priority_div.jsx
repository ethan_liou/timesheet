var React = require('react');

module.exports = React.createClass({
  getPriority(priority){
    switch(priority){
      case 1:
      return "low";
      case 2:
      return "mid";
      case 3:
      return "high";
    }
  },
  getStyle(){
    return {
      high:{color:"red"},
      mid:{color:"blue"},
      low:{color:"green"},
    }
  },
  render() {
    var priority = this.getPriority(this.props.priority);
    var style = this.getStyle()[priority];
    return (
      <div style={style}>{priority}</div>
    );
  },
});
