var React = require('react');
var mui = require('material-ui');
var {
  Snackbar
} = mui;
var Colors = mui.Styles.Colors;
var ToastStore = require('../stores/ToastStore');

module.exports = React.createClass({
  getInitialState(){
    return {text:"Test"};
  },
  _onChange(){
    this.refs.snack.dismiss();
    var info = ToastStore.get();
    this.setState({text:info.text, level:info.type, actionFunc:info.actionFunc});
    this.refs.snack.show();
  },
  componentDidMount() {
    ToastStore.addChangeListener(this._onChange);
  },
  componentWillUnmount() {
    ToastStore.removeChangeListener(this._onChange);
  },
  render() {
    return (
      <Snackbar
        ref="snack"
        action={this.state.actionFunc == null ? "" : "undo"}
        onActionTouchTap={this.state.actionFunc}
        message={this.state.text}
        autoHideDuration={3000}/>
    );
  },
});
