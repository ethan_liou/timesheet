var React = require('react');
var Router = require('react-router');
var { List, Styles, ListItem } = require('material-ui');
var { Colors, Spacing, Typography } = Styles;
var ProjStore = require("../stores/ProjStore");
var CompanyStore = require("../stores/CompanyStore");

module.exports = React.createClass({
  getInitialState(){
    return {
      projects : [],
      members: [],
      private_projects : []
    };
  },
  contextTypes:{
    router: React.PropTypes.func
  },
  getStyles() {
    return {
      cursor: 'pointer'
    };
  },
  componentWillUnmount() {
    ProjStore.removeChangeListener(this._onChange);
    CompanyStore.removeChangeListener(this._onChange);
  },
  _onChange(){
    var projects = $$$$.map(ProjStore.getAll(), function(project, pid){
      return [{...project, project_id:pid}];
    });

    var members = $$$$.map(CompanyStore.getMembers(), function(member, mid){
      return member.self ? null : [{...member, member_id:mid}];
    });
    this.setState({projects:projects, members:members});
  },
  componentDidMount() {
    ProjStore.addChangeListener(this._onChange);
    CompanyStore.addChangeListener(this._onChange);
    this._onChange();
  },
  _goRoute(route){
    this.context.router.transitionTo(route);
  },
  render() {
    var transBg = {backgroundColor:"transparent"};
    var subheaderStyle = {color:Colors.grey500, lineHeight:"32px"};
    return (
      <div style={this.props.style}>
        <List style={transBg}>
          <ListItem onClick={this._goRoute.bind(null, "/")} innerDivStyle={{paddingLeft:"20px", paddingTop:"0px", paddingBottom:"0px"}} primaryText={<div style={subheaderStyle}>My todo</div>} />
        </List>
        <List style={transBg} subheader="PROJECTS" subheaderStyle={subheaderStyle}>
          {this.state.projects.map(function(proj){
            return <ListItem onClick={this._goRoute.bind(null, "/projectview/"+proj.project_id)} innerDivStyle={{paddingLeft:"20px", paddingTop:"0px", paddingBottom:"0px"}} key={proj.project_id} primaryText={<div style={subheaderStyle}>{proj.project_name}</div>}  />
          }.bind(this))}
        </List>
        <List style={transBg} subheader="MEMBERS" subheaderStyle={subheaderStyle}>
          {this.state.members.map(function(member){
            return <ListItem onClick={this._goRoute.bind(null, "/userview/"+member.member_id)} innerDivStyle={{paddingLeft:"20px", paddingTop:"0px", paddingBottom:"0px"}} key={member.member_id} primaryText={<div style={subheaderStyle}>{member.member_name}</div>}  />
          }.bind(this))}
        </List>
        <List style={transBg} subheader="PRIVATE PROJECTS" subheaderStyle={subheaderStyle}>

        </List>
      </div>
    );
  }
});
