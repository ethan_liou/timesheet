var React = require('react');
var update = require('react/lib/update');
var mui = require('material-ui');
var CompanyStore = require('../stores/CompanyStore');
var ProjStore = require('../stores/ProjStore');
var TaskAction = require('../actions/TaskAction');
var PropTypes = React.PropTypes;
var {
  Dialog,
  TextField,
  DatePicker,
  SelectField,
  Styles,
  Slider
} = mui;
var Colors = mui.Styles.Colors;
var AutoCompletedTextField = require('./autocompleted_textfield.jsx');

module.exports = React.createClass({
  mixins: [React.addons.LinkedStateMixin],
  propTypes: {
    time_allocation: PropTypes.bool.isRequired
  },
  getDefaultProps(){
    return {
      time_allocation:false,
      project_id:null
    }
  },
  getInitialState(){
    return {members:[], projects:[]};
  },
  getStyle(){
    return {
      tdStyle:{
        padding:"4px"
      },
      labelStyle:{
        lineHeight:"20px",
        fontSize:"13px",
        color:Colors.grey600
      }
    }
  },
  _onChange(){
    var self = null;
    var members = $$$$.map(ProjStore.getMembers(), function(member, mid){
      if(member.self){
        self = [mid, member];
      }
      return [{name:member.name, id:mid, search:member.name.toLowerCase()}];
    });
    var projects = $$$$.map(ProjStore.getAll(), function(proj, pid){
      return [{text:proj.project_name, payload:pid}];
    });
    var newState = {members:members, projects:projects};
    if(self){
      newState.self_id = self[0];
      newState.self_name = self[1].name;
    }
    this.setState(newState);
  },
  componentDidMount(){
    this._onChange();
    CompanyStore.addChangeListener(this._onChange);
    ProjStore.addChangeListener(this._onChange);
  },
  componentWillUnmount(){
    CompanyStore.removeChangeListener(this._onChange);
    ProjStore.removeChangeListener(this._onChange);
  },
  show(task){
    var newState = {};
    if(task){
      newState = task;
      console.log(task);
    } else {
      newState = {
        task_name:"",
        requester:this.state.self_id,
        task_owner_id:this.state.self_id,
        priority:2,
        enabled:0,
        status:'todo',
        task_deadline:new Date(),
        time_allocation:0.2
      };
      if(this.props.project_id){
        newState.project_id = this.props.project_id;
      }
    }
    this.setState(newState);
    this.refs.addDialog.show();
  },
  _onChangeSlider(e, value){
    this.setState({time_allocation : value});
  },
  _onAdd(){
    TaskAction.add(this.state, function(){ 
      this.refs.addDialog.dismiss(); 
    }.bind(this));
  },
  render() {
    var style = this.getStyle();
    return (
        <Dialog 
          ref="addDialog"
          title="Add Task"
          actions={[
            { text: 'Cancel' },
            { text: 'Add', onTouchTap: this._onAdd}
            ]}>
          <table width="100%">
            <tr>
              <td style={style.tdStyle}>
                <p style={style.labelStyle}>Task name</p>
                <TextField
                  valueLink={this.linkState('task_name')}
                  fullWidth={true}/>
              </td>
              <td style={style.tdStyle}>
                <p style={style.labelStyle}>Due date</p>
                <DatePicker
                  autoOk={true}
                  value={this.state.task_deadline}
                  onChange={function(e,date){ this.setState({task_deadline:date}) }.bind(this)}
                  mode="landscape"
                  fullWidth={true}/>
              </td>
            </tr>
            <tr>
              <td style={style.tdStyle}>
                <p style={style.labelStyle}>Task priority</p>
                <SelectField
                  valueLink={this.linkState('priority')}
                  fullWidth={true}
                  menuItems={[
                    {payload:1, text:"Low"},
                    {payload:2, text:"Medium"},
                    {payload:3, text:"High"}
                    ]}/>
              </td>
              <td style={style.tdStyle}>
                <p style={style.labelStyle}>Project</p>
                <SelectField
                  disabled={this.props.project_id !== null}
                  menuItems={this.state.projects}
                  valueLink={this.linkState('project_id')}
                  fullWidth={true}/>
              </td>
            </tr>
            <tr>
              <td style={style.tdStyle}>
                <p style={style.labelStyle}>Assign to</p>
                <AutoCompletedTextField
                  defValue={this.state.self_name}
                  onSuggest={function(candidate){ this.state.task_owner_id = candidate.id }.bind(this) }
                  candidates={this.state.members}/>
              </td>
              <td style={style.tdStyle}>
                <p style={style.labelStyle}>Requester</p>
                <AutoCompletedTextField
                  defValue={this.state.self_name}
                  onSuggest={function(candidate){ this.state.requester = candidate.id }.bind(this) }
                  candidates={this.state.members}/>
              </td>
            </tr>
          </table>
          <br/>
          <br/>
          {this.props.time_allocation && (
          <div style={style.tdStyle}>
            <Slider 
              name="time_allocation"
              description={"Time allocation : " + (10 * this.state.time_allocation) + " Hours"}
              value={this.state.time_allocation}
              step={0.05}
              onChange={this._onChangeSlider} />
          </div>
          )}
        </Dialog>
    );
  },
});
