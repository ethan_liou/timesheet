var React = require('react');
var PropTypes = React.PropTypes;
var DropTarget = require('react-dnd').DropTarget;
var DragItem = require('../constants/DragItem');
var TaskStore = require('../stores/TaskStore');
var UserTask = require("./user_task.jsx");
var {
  FloatingActionButton,
  Styles
} = require('material-ui');
var Colors = Styles.Colors;

function getTasks(props){
    return $$$$.map(TaskStore.getAll(), function(task, task_id){
        for(var prop_k in props){
            if(task[prop_k] != props[prop_k]) return null;
        }
        return task;
    });
}

var TaskContainer = React.createClass({
    propTypes: {
        virtualSize: PropTypes.object.isRequired,
        connectDropTarget: PropTypes.func.isRequired,
        isOver: PropTypes.bool.isRequired,
        canDrop: PropTypes.bool.isRequired,
        filter: PropTypes.object.isRequired,
        userview: PropTypes.bool.isRequired
    },
    getInitialState(){
        return {tasks:getTasks(this.props.filter)};
    },
    componentWillUnmount() {
        TaskStore.removeChangeListener(this._onChange);
    },
    _onChange(){
        this.setState({tasks:getTasks(this.props.filter)});
    },
    componentDidMount() {
        TaskStore.addChangeListener(this._onChange);
    },
    getStyles(){
        return {
            card:{
                marginTop:"5px",
                marginBottom:"5px"
            },
            virtualCard:{
                marginTop:"5px",
                marginBottom:"5px",
                backgroundColor: Colors.grey300            
            }
        };
    },
    render() {
        var styles = this.getStyles();
        return this.props.connectDropTarget(
            <div>
                {this.state.tasks.map(function(task){
                    return (<UserTask onClick={this.props.onClickItem.bind(null, task)} userview={this.props.userview} style={styles.card} key={task.task_id} task={task} />);
                }.bind(this))}
                <div style={{...styles.virtualCard, ...this.props.virtualSize}}></div>
            </div>
        );
    }
});

var spec = {
    drop: function (props, monitor, component) {
        return {action: props.filter};
    }
}

function collect(connect, monitor) {
    return {
        virtualSize : monitor.getItem() == null ? {width:0, height:0} : monitor.getItem().size,
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}

module.exports = DropTarget(DragItem.TASK, spec, collect)(TaskContainer);
