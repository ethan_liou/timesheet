var React = require('react');

module.exports = React.createClass({
  getStyle(){
    return {
      todo:{color:"green"},
      doing:{color:"red"}
    }
  },
  render() {
    var style = this.getStyle()[this.props.status];
    return (
      <div style={style}>{this.props.status}</div>
    );
  },
});
