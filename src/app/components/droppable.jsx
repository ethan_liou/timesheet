var React = require('react');
var PropTypes = React.PropTypes;
var DropTarget = require('react-dnd').DropTarget;
var DragItem = require('../constants/DragItem');
var {
  FloatingActionButton,
} = require('material-ui');

var Droppable = React.createClass({
    propTypes: {
        action: PropTypes.string.isRequired,
        connectDropTarget: PropTypes.func.isRequired,
        isOver: PropTypes.bool.isRequired,
        canDrop: PropTypes.bool.isRequired
    },
    getInitialState(){
        return {};
    },
    render() {
        return this.props.connectDropTarget(
          <FloatingActionButton backgroundColor={ this.props.isOver ? "green" : "red" } style={{ ...this.props.eleStyle, display : this.props.canDrop ? "block" : "none" }}>
            {this.props.children}
          </FloatingActionButton>
        );
    }
});

var spec = {
    drop: function (props, monitor, component) {
      return {action: {status:props.action}};
    }
}

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}

module.exports = DropTarget(DragItem.TASK, spec, collect)(Droppable);
