var React = require('react');
var { MenuItem, LeftNav, Styles, Avatar, ClearFix } = require('material-ui');
var { Colors, Spacing, Typography } = Styles;
var ProjStore = require("../stores/ProjStore");
var DragSource = require('react-dnd').DragSource;
var DragItem = require('../constants/DragItem');
var PropTypes = React.PropTypes;
var TaskAction = require("../actions/TaskAction");

var spec = {
    beginDrag: function (props, monitor, component) {
      console.log(props);
      return {memberId:props.memberId};
    },
    endDrag: function(props, monitor) {
      var res = monitor.getDropResult();
      if(res){
        TaskAction.change_attr(res.task, "task_owner_id", props.memberId);
      }
    }
}

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    };
}

var DraggableAvatar = DragSource(DragItem.MEMBER, spec, collect)(React.createClass({
  propTypes: {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    memberId: PropTypes.string.isRequired
  },
  render(){
    return this.props.connectDragSource(<Avatar memberId={this.props.memberId} style={this.props.style}>{this.props.children}</Avatar>);
  }
}));

var MemberObj = React.createClass({
  getStyles(){
    return {
      root:{
        paddingBottom:"12px"
      },
      avatar:{
        float:"left"
      },
      name:{
        float:"left",
        marginLeft:"8px",
        lineHeight:"40px"
      }
    }
  },
  render(){
    
    var styles = this.getStyles();
    return (<div style={styles.root}>
      <ClearFix>
        <DraggableAvatar style={styles.avatar} memberId={this.props.member.id}>{this.props.member.name[0]}</DraggableAvatar>
        <div style={styles.name}>{this.props.member.name}</div>
      </ClearFix>
    </div>);
  }
});

module.exports = React.createClass({
  getInitialState(){
    return {
      members : []
    };
  },
  getStyles() {
    return {
    };
  },
  componentWillUnmount() {
    ProjStore.removeChangeListener(this._onChange);
  },
  _onChange(){
    var members = $$$$.map(ProjStore.getMembers(), function(member, mid){
      return [{name:member.name, id:mid}];
    });
    this.setState({members:members});
  },
  componentDidMount() {
    this._onChange();
    ProjStore.addChangeListener(this._onChange);
  },
  render() {
    return (
      <div style={this.props.style}>
        <p>Members</p>
        {
          this.state.members.map(function(member){
            return <MemberObj member={member} key={member.id}/>;
          }.bind(this))
        }
      </div>
    );
  }
});
