var React = require('react');
var { AppBar } = require('material-ui');
var AppbarStore = require('../stores/AppbarStore');

module.exports = React.createClass({
  getInitialState(){
    return {breadcrumbs:["Timesheet"]};
  },
  componentWillUnmount() {
    AppbarStore.removeChangeListener(this._onChange);
  },
  _onChange(){
    this.setState({breadcrumbs:AppbarStore.getBreadcrumb()});
  },
  componentDidMount() {
    AppbarStore.addChangeListener(this._onChange);
  },
  render() {
    return (
      <div>
        <h5>Section 1</h5>
        <div className="divider"></div>
      </div>
    );
  },
});
