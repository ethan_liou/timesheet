var React = require('react');
var mui = require('material-ui');
var {
  TextField, List, ListItem, Paper, ListDivider
} = mui;

module.exports = React.createClass({
  getInitialState(){
    return {word:"", show_candidates:this.props.candidates};
  },
  componentWillReceiveProps(props){
    this.setState({word:"", show_candidates:this.props.candidates});
  },
  _onChange(e){
    if(this.state.word == "" && e.target.value != ""){
      this.setMenuPositions();
    }
    this.setState({
      word:e.target.value,
      show_candidates:this.props.candidates.filter(function(candidate){
        return candidate.search.startsWith(e.target.value.toLowerCase());
      })
    });
  },
  setMenuPositions(){
    var node = React.findDOMNode(this.refs.input);
    var rect = node.getBoundingClientRect()
    this.setState({
      menuTop: node.offsetTop + node.offsetHeight,
      menuLeft: node.offsetLeft,
      menuWidth: node.offsetWidth,
      menuMaxHeight: $$$$(window).height() - rect.bottom
    })
  },
  _onClickAction(candidate){
    this.refs.input.setValue(candidate.name);
    this.setState({word:""});
    this.props.onSuggest(candidate);
  },
  _renderMenu(){
    return (
      <Paper zDepth={1} style={{ zIndex:"999", position: 'fixed', overflow:"auto", maxHeight:this.state.menuMaxHeight, left: this.state.menuLeft, top: this.state.menuTop, minWidth: this.state.menuWidth, display: this.state.word == "" ? "none" : "block" }}>
        <List style={{paddingTop:"0px", paddingBottom:"0px"}}>
          {this.state.show_candidates.map(function(candidate, idx){
            return <ListItem onClick={this._onClickAction.bind(null, candidate)} key={"c"+idx} primaryText={candidate.name} />
          }.bind(this))}
          <ListDivider />
          <ListItem key={"cadd"} primaryText={"Invite "+ this.state.word + " as new member"} />
        </List>
      </Paper>
    );
  },
  render() {
    return (
      <div>
        <TextField ref="input" onChange={this._onChange} defaultValue={this.props.defValue} fullWidth={true} />
        {this.refs.input && this._renderMenu()}
      </div>
    );
  },
});
