var React = require('react');
var { TextField } = require('material-ui');

module.exports = React.createClass({
  mixins: [React.addons.LinkedStateMixin],
  getInitialState(){
    return {
        task_name:""
    }
  },
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <TextField
              valueLink={this.linkState('task_name')}
              fullWidth={true}/>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-6">
            <TextField
              valueLink={this.linkState('task_name')}
              fullWidth={true}/>
          </div>
        </div>
      </div>
    );
  }
});