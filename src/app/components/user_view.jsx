var React = require('react');
var update = require('react/lib/update');
var mui = require('material-ui');
var ThemeManager = new mui.Styles.ThemeManager();
var Colors = mui.Styles.Colors;
var { Spacing, Colors } = mui.Styles;
var TaskStore = require('../stores/TaskStore');
var {
  ClearFix,
  Card,
  CardHeader,
  Avatar,
  CardText,
  CardActions,
  FlatButton,
  RaisedButton,
  FloatingActionButton,
  CardTitle,
} = mui;
var Add = require('material-ui/lib/svg-icons/content/add');
var Clear = require('material-ui/lib/svg-icons/content/clear');
var Done = require('material-ui/lib/svg-icons/action/done');
var AddTaskDialog = require('./add_task_dlg.jsx');
var ToastView = require("./toast_view.jsx");
var TaskContainer = require("./task_container.jsx");
var Droppable = require("./droppable.jsx");
var TaskAction = require("../actions/TaskAction");
var CompanyStore = require("../stores/CompanyStore");

module.exports = React.createClass({
  getStyles(){
    return {
      card:{
        margin:"5px"
      },
      addBtn:{
        position:'fixed',
        right: '24px',
        bottom: '24px'
      },
      removeBtn:{
        position:'fixed',
        right: '124px',
        bottom: '24px'
      },
      doneBtn:{
        position:'fixed',
        right: '24px',
        bottom: '24px'
      }
    };
  },
  _addTask(){
    this.refs.add_task_dlg.show();
  },
  _reloadData(){
    TaskAction.list({task_owner_id:this.state.member.member_id, status:"todo,doing"});
  },
  getInitialState(){
    var mid = this.props.params.id;
    if(!mid){
      mid = CompanyStore.getSelfId();
    }
    return {member:{...CompanyStore.getMember(mid), member_id:mid}};
  },
  componentWillMount(){
    this._reloadData();
  },
  componentWillReceiveProps(nextProps){
    var mid = nextProps.params.id; 
    if(!mid){
      mid = CompanyStore.getSelfId();
    }
    this.setState({member:{...CompanyStore.getMember(mid), member_id:mid}}, function(){this._reloadData();}.bind(this));
  },
  _editTask(task){
    this.refs.add_task_dlg.show(task);
  },
  render() {
    var styles = this.getStyles();
    return (
      <div>
        <h5>Member : {this.state.member.member_name}</h5>
        <div className="divider"></div>
        <FloatingActionButton style={styles.addBtn} onClick={this._addTask}>
          <Add />
        </FloatingActionButton>
        <Droppable eleStyle={styles.removeBtn} action="done">
          <Done />
        </Droppable>
        <Droppable eleStyle={styles.doneBtn} action="deleted">
          <Clear />
        </Droppable>
        <ToastView/>
        <div className="row">
          <div className="col s8">
            <p>Doing</p>
            <TaskContainer onClickItem={this._editTask} userview={true} filter={{status:"doing"}}/>
          </div>
          <div className="col s4">
            <p>Pending</p>
            <TaskContainer onClickItem={this._editTask} userview={true} filter={{status:"todo"}}/>
          </div>
        </div>
        <AddTaskDialog ref="add_task_dlg"/>
      </div>
    );
  },
});