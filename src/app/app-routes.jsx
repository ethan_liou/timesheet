var React = require('react');
var Router = require('react-router');
var Route = Router.Route;
var Redirect = Router.Redirect;
var DefaultRoute = Router.DefaultRoute;

var Master = require("./components/master.jsx");
var UserView = require("./components/user_view.jsx");
var ProjectView = require("./components/project_view.jsx");
var AddProjView = require("./components/add_proj.jsx");

module.exports = (
  <Route name="root" path="/" handler={Master}>
    <Route name="userview" path="userview/:id" handler={UserView} />
    <Route name="projectview" path="projectview/:id" handler={ProjectView} />
    <DefaultRoute handler={UserView}/>
  </Route>
);
