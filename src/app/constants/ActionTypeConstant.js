var keyMirror = require('keymirror');

module.exports = keyMirror({
    ADD_TASK: null,
    LIST_TASK:null,
    CHANGE_TASK_STATUS:null,
    CHANGE_TASK_ENABLED:null,
    TASK_CHANGE_ATTR:null,
    TASK_CLEAR:null,
    ERROR:null,
    INFO:null,
    COMPANY_LIST_MEMBER:null,
    PROJECT_LIST:null,
    PROJECT_MEMBER_LIST:null,
    APPBAR_SET_BREADCRUMB:null
});
