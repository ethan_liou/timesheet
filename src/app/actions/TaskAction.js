var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypeConstant = require('../constants/ActionTypeConstant');
var ToastAction = require('./ToastAction');

module.exports = {
  change_attr: function(task, k, v){
    console.log(task,k,v);
    var oriVal = task[k];
    task[k] = v;
    AppDispatcher.dispatch({
        task: task,
        type: ActionTypeConstant.TASK_CHANGE_ATTR
    });
    $$$$.ajax({
      url : "task/change_attr",
      type : "POST",
      data : JSON.stringify({task_id:task.task_id, attr:{[k]:v}}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(){
        ToastAction.info(k + " of " + task.task_name + " is "+v, function(){
          AppDispatcher.dispatch({
              task: $$$$.extend(task,{[k]:oriVal}),
              type: ActionTypeConstant.TASK_CHANGE_ATTR
          });
          // rollback
          $$$$.ajax({
            url : "task/change_attr",
            type : "POST",
            data : JSON.stringify({task_id:task.task_id, attr:{[k]:oriVal}}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(){
              ToastAction.info(k + " of " + task.task_name + " is "+oriVal);
            },
            error: function(){
              ToastAction.error("Cannot undo");
            }
          });
        });
      },
      error: function(){
        AppDispatcher.dispatch({
            task: $$$$.extend(task,{[k]:oriVal}),
            type: ActionTypeConstant.TASK_CHANGE_ATTR
        });
        ToastAction.error(task.task_name + " cannot modifed");
      }
    });      
  },
  list: function(params){
    AppDispatcher.dispatch({
        type: ActionTypeConstant.TASK_CLEAR
    });
    $$$$.get("task/list", params, function( data ){
        AppDispatcher.dispatch({
            type: ActionTypeConstant.LIST_TASK,
            tasks: data
        });
    });
  },
  add: function(task, callback){
    $$$$.ajax({
      url : "task/add",
      type : "POST",
      data : JSON.stringify(task),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(res){
        callback();
        task.task_id = res.task_id;
        AppDispatcher.dispatch({
            type: ActionTypeConstant.ADD_TASK,
            task_id: res.task_id,
            task: task
        });
      }
    });
  }
};
