var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypeConstant = require('../constants/ActionTypeConstant');

module.exports = {
  list: function(){
    $$$$.get("project/list?field=project_name", function( data ){
      AppDispatcher.dispatch({
          type: ActionTypeConstant.PROJECT_LIST,
          projects: data
      });
    });
  },
  listMembers: function(projectId){
    $$$$.get("project/members/"+projectId, function( data ){
      AppDispatcher.dispatch({
          type: ActionTypeConstant.PROJECT_MEMBER_LIST,
          members: data
      });
    });
  }
};
