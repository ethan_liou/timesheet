var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypeConstant = require('../constants/ActionTypeConstant');

module.exports = {
  error: function(errorText, actionFunc){
    AppDispatcher.dispatch({
        type: ActionTypeConstant.ERROR,
        text: errorText,
        actionFunc: actionFunc
    });
  },
  info: function(infoText, actionFunc){
    AppDispatcher.dispatch({
        type: ActionTypeConstant.INFO,
        text: infoText,
        actionFunc: actionFunc
    });
  }
};
