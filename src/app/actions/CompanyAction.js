var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypeConstant = require('../constants/ActionTypeConstant');

module.exports = {
  listMembers: function(){
    $$$$.get("company/list_members?field=member_name", function( data ){
      AppDispatcher.dispatch({
          type: ActionTypeConstant.COMPANY_LIST_MEMBER,
          members: data
      });
    });
  }
};
