var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypeConstant = require('../constants/ActionTypeConstant');

module.exports = {
  setBreadcrumb: function(breadcrumbs){
    AppDispatcher.dispatch({
        type: ActionTypeConstant.APPBAR_SET_BREADCRUMB,
        breadcrumbs: breadcrumbs
    });
  }
};
