var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ActionTypeConstant = require('../constants/ActionTypeConstant');
var PROJ_EVENT = "PROJ";

var _projects = {};
var _members = {};

var ProjStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(PROJ_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(PROJ_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(PROJ_EVENT, callback);
  },
  getAll: function() {
    return _projects;
  },
  getMembers: function(){
    return _members;
  },
  getProject: function(proj_id){
    return _projects[proj_id];
  }
});

ProjStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.type){
    case ActionTypeConstant.PROJECT_LIST:
      _projects = action.projects;
      ProjStore.emitChange();
      break;      
    case ActionTypeConstant.PROJECT_MEMBER_LIST:
      _members = action.members;
      ProjStore.emitChange();
      break;      
  }
});

module.exports = ProjStore;
