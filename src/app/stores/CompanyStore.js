var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ActionTypeConstant = require('../constants/ActionTypeConstant');
var COMPANY_EVENT = "COMPANY";

var _members = {};

var CompanyStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(COMPANY_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(COMPANY_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(COMPANY_EVENT, callback);
  },
  getSelfId: function(){
    return $$$$.map(_members, function(member,mid){ return member.self ? mid : null; })[0]
  },
  getMembers: function() {
    return _members;
  },
  getMember: function(mid) {
    return _members[mid];
  }
});

CompanyStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.type){
    case ActionTypeConstant.COMPANY_LIST_MEMBER:
    {
      _members = action.members;
      CompanyStore.emitChange();
      break;      
    }
  }
});

module.exports = CompanyStore;
