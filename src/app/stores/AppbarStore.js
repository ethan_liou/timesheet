var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ActionTypeConstant = require('../constants/ActionTypeConstant');
var APPBAR_STORE = "APPBAR";

var _breadcrumb = [];

var AppbarStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(APPBAR_STORE);
  },

  addChangeListener: function(callback) {
    this.on(APPBAR_STORE, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(APPBAR_STORE, callback);
  },
  getBreadcrumb: function() {
    return _breadcrumb;
  },
});

AppbarStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.type){
    case ActionTypeConstant.APPBAR_SET_BREADCRUMB:
    {
      _breadcrumb = action.breadcrumbs;
      AppbarStore.emitChange();
      break;      
    }
  }
});

module.exports = AppbarStore;
