var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ActionTypeConstant = require('../constants/ActionTypeConstant');
var TOAST_EVENT = "TOAST";

var _info = null;

var ToastStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(TOAST_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(TOAST_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(TOAST_EVENT, callback);
  },
  get: function() {
    return _info;
  },
});

ToastStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.type){
    case ActionTypeConstant.INFO:
    case ActionTypeConstant.ERROR:
    {
      _info = action;
      ToastStore.emitChange();
      break;      
    }
  }
});

module.exports = ToastStore;
