var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var ActionTypeConstant = require('../constants/ActionTypeConstant');

var CHANGE_EVENT = "CHANGE";

var _tasks = {};

var TaskStore = assign({}, EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  get: function(id) {
    return _tasks[id];
  },

  getAll: function() {
    return _tasks;
  }
});

TaskStore.dispatchToken = AppDispatcher.register(function(action) {
  switch(action.type){
    case ActionTypeConstant.LIST_TASK:
      _tasks = action.tasks;
      TaskStore.emitChange();
      break;
    case ActionTypeConstant.ADD_TASK:
      _tasks[action.task_id] = action.task;
      TaskStore.emitChange();
      break;
    case ActionTypeConstant.TASK_CHANGE_ATTR:
      if(["deleted","done"].indexOf(action.task.status) >= 0){
        delete _tasks[action.task.task_id];
      } else {
        _tasks[action.task.task_id] = action.task;        
      }
      TaskStore.emitChange();
      break;
    case ActionTypeConstant.TASK_CLEAR:
      _tasks = {};
      TaskStore.emitChange();
      break;
  }
});

module.exports = TaskStore;
