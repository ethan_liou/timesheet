(function () {
  var React = require('react');
  var Router = require('react-router');
  var AppRoutes = require('./app-routes.jsx');
  var injectTapEventPlugin = require('react-tap-event-plugin');
  window.React = React;
  window.$$$$ = require('jquery');
  injectTapEventPlugin();
  Router
    .create({
      routes: AppRoutes,
      scrollBehavior: Router.ScrollToTopBehavior
    })
    .run(function (Handler) {
      React.render(<Handler/>, document.getElementById('app'));
    });
})();
